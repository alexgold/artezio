"""at startup, asks the user for the address of the site that needs to be checked"""

from website_alive.check_response import check_res

USER_WEBSITE = input('Enter website name starting with http:// (ex. http://google.com): ')
if check_res(USER_WEBSITE) is True:
    print("Website {} is available".format(USER_WEBSITE))
else:
    print("Website {} is not available".format(USER_WEBSITE))
