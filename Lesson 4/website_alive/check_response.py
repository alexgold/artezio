"""Module with function which checks the response"""

import requests
from website_alive.make_request import do_request


def check_res(user_website):
    """function checks the response"""
    condition = do_request(user_website).status_code
    return condition == requests.codes.ok
