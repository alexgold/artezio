"""Module with function which makes a request"""

import requests


def do_request(user_website):
    """function makes a request"""
    req = requests.get(user_website)
    return req
