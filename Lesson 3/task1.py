def squares(collection):
    squared = []
    for i in collection:
        squared.append(i*i)
    return squared


def even(collection):
    evenly = []
    for i in range(2, len(collection), 2):
        evenly.append(collection[i])
    return evenly


def cube(collection):
    cubed = []
    for i in range(1, len(collection), 2):
        if collection[i] % 2 != 0:
            cubed.append(collection[i]**3)
    return cubed


collection = [1, 2, 3, 5, 5, 6]
print(squares(collection))
print(even(collection))
print(cube(collection))
