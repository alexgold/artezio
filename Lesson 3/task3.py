def average(a, b, c, d, /):
    global historic_max
    mean = (a + b + c + d) / 4
    historic_max = max(historic_max, max(a, b, c, d))
    return mean, historic_max


historic_max = float('-inf')
print(average(-5, -4, -3, -2))
print(average(-5, -4, -3, 10))
print(average(-5, -4, -3, 9))
