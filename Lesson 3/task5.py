def closest_to_zero(user_str):
    numbers = []
    for chunk in user_str.strip().replace(',','').split(' '):
        try:
            numbers.append(float(chunk))
        except ValueError:
            pass
    current_min = abs(numbers[0])
    min_index = 0
    for i, val in enumerate(numbers[1:]):
        if abs(val) < current_min:
            current_min = abs(val)
            min_index = i + 1

    return numbers[min_index]


print(closest_to_zero('1 2 -0.5 0.75 22'))
