def find_diff(list1, list2):
    diff_list = []
    if len(list1) >= len(list2):
        [diff_list.append(val) for val in list1 if val not in list2]
    else:
        [diff_list.append(val) for val in list2 if val not in list1]
    return diff_list


list2 = [13, 25, 34, 42, 53, 64, 78, 87, 96, 90]
list1 = [13, 25, 34, 42, 53, 64, 78, 87, 96, 90, 1, 2, 3]

print (find_diff(list1, list2))