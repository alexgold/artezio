def gen_dic(n):
    return {x:x**2 for x in range(1, n+1)}


print(gen_dic(5))