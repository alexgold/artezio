import collections


string = input('Enter string: ')
c = collections.Counter()
for word in string:
    c[word] += 1

result = dict(c)
print(result)
