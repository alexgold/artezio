import itertools


def filter(iterable):
    filtered = itertools.filterfalse(func_for_filter, iterable)
    return [i for i in filtered]

def func_for_filter(iterable):
    if len(iterable) >= 2:
        return iterable[0] != iterable[-1]
    else:
        return True


SampleList = ['b', 'abc', 'xyz', 'aba', '1221', 'a']
result = filter(SampleList)
print(result)
