def subset(x, y, z):
    return True if (z < x and z != x) or (z < y and z != y) else False


x = set([1, 2])
y = set([3, 4])
z = set([5])
print(subset(x, y, z))
