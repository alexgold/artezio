def merge_dic(dictionary1, dictionary2):
    new_dic = dictionary1.copy()
    new_dic.update(dictionary2)
    return new_dic


dic1 = {1: 'a', 2: 'b', 3: 'c'}
dic2 = {'a': 1, 'b': 2, 'c': 3}

print(merge_dic(dic1, dic2))