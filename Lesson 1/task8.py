def find_max(dictionary):
    list_values = list(dictionary.values())
    list_values.sort(reverse=True)
    top3_values = list_values[:3]
    return top3_values


dic2 = {'a': 4, 'b': 5, 'c': 3, 'd': 4}
print(find_max(dic2))