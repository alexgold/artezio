def like_range(a, b, c=None):
    if c is None:
        c = 1
    elif c == 0:
        return print('ValueError: arg 3 must not be zero')
    range_list = []
    if a < b and c > 0:
        while a < b:
            range_list.append(a)
            a += c
    elif a > b and c < 0 :
        while a > b:
            range_list.append(a)
            a += c
    return range_list


like_range(100, 10, -1)
