def find_quantity_dividends(a, b, c):
    counter = 0
    if a > b:
        while a > b:
            a -= 1
            if a % c == 0:
                counter += 1
    elif a < b:
        while a < b:
            b -= 1
            if b % c == 0:
                counter += 1
    return counter


print(find_quantity_dividends(5, 1, 2))
